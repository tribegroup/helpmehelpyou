# HelpMeHelpYou

This is the GOTO location for volunteers, employees or business partners looking for ways to contribute to the work 
of TribeGroup or any of its affiliates.
It is also the central place for anyone affiliated with TribeGroup to list tasks that they need help on.


# People who need help and what for

## Paul Henckel
In his role as daily manager of WorkHive Siljangade 1, he needs help with the following:
1. ON-SITE
    1. [ ] Steam cleaning, decorating, fixing lamps, various fixes
    1. [ ] IT: fixing printer, router, server
1. AD-HOC
    1. Legal:
        1. [ ] Formulate Membership agreement/ToS 
[examples](https://drive.google.com/drive/folders/1PMwfxFG3TTgHFu63fQvNtTPjjB07-sMo?usp=sharing)
        1. [ ] Formulate Community Guidelines 
[examples](https://drive.google.com/drive/folders/1PMwfxFG3TTgHFu63fQvNtTPjjB07-sMo?usp=sharing)
    1. Research: 
        1. [ ] What is the most reasonable insurance plan for a small coworking space?
        1. [ ] Should I use ledger-cli.org, e-conomic.dk or xero.com for accounting and why?
    1. Accounting
        1. [ ] Type in all receipts in accounting system before mid february


In his role as co-developer with Fantsuam.org in creating Community Network infrastructure in Kafanchan, Nigeria.
1. AD-HOC
    1. Fundraising: 
        - [ ] librerouter.org
        - [ ] pslab.io kits
        - [ ] solar power system
    1. Research & Development:
        - [ ] How to use librerouter on TV white space for freifunk connectivity and what are the pros/cons a regular 
WiFi freifunk network?
        - [ ] What is the most reliable and cost effective solar power system?
        - [ ] Prototype of a way-cooler based low-power linux distro in 3 variants: 1) thin-client server, 2) 
sugar-on-a-stick style of portable learning environment, 3) olpc style of auto mesh network for multi-computer 
collaboration.
        - [ ] Developing a knowledge base around sociocracy, permaculture/regrarianism, interaction+participatory 
design, and good technology (C3)
